FROM alpine:3.9
RUN set -eu && \
        apk add --no-cache python3 && \
        python3 -m pip install -U pip && \
        python3 -m pip install pipenv

ADD Pipfile* /app/
WORKDIR /app
RUN pipenv install

ENV DJANGO_SETTINGS=url_shortener.settings
ADD . /app/
RUN pipenv run python manage.py migrate
CMD pipenv run gunicorn --bind 0.0.0.0:8000 url_shortener.wsgi:application
