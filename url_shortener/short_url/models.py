from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

from .managers import ShortenedUrlQuerySet
from .utils import encode_int


class ShortenedUrl(models.Model):
    url = models.TextField(_("URL"))

    objects = ShortenedUrlQuerySet.as_manager()

    @property
    def short_code(self):
        if not self.pk:
            return ""
        return encode_int(self.pk)

    def clean(self):
        self.url = self.url.strip()
        if "\n" in self.url or "\r" in self.url:
            raise ValidationError({"url": _("A URL cannot contain newlines.")})

    def __str__(self):
        return f"short_url={self.short_code}"
