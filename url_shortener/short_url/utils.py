import base64


def encode_int(value):
    if value is None or value == 0:
        raise OverflowError("value > 0")
    value_as_bytes = value.to_bytes(4, byteorder="big")
    # Strip starting 0 bytes for shorter codes without losing info
    value_as_bytes = value_as_bytes.lstrip(b"\x00")
    return base64.urlsafe_b64encode(value_as_bytes).decode("ascii").rstrip("=")


def decode_int(value):
    padding = len(value) % 4
    return int.from_bytes(
        base64.urlsafe_b64decode(value + "=" * padding), byteorder="big"
    )
