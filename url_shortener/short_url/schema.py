import graphene
from graphene_django import DjangoObjectType
from graphene_django.forms.mutation import DjangoModelFormMutation


from .forms import ShortenedUrlForm
from .models import ShortenedUrl as ShortenedUrlModel


class ShortenedUrl(DjangoObjectType):
    """Model of a short code to the full URL"""

    class Meta:
        model = ShortenedUrlModel
        fields = ("url",)

    short_code = graphene.String()


class Query(graphene.ObjectType):
    """Query the shortened URLs.

    You can find the mapping of a specific short code."""

    url = graphene.Field(ShortenedUrl, short_code=graphene.String())

    def resolve_url(self, info, short_code):
        try:
            return ShortenedUrlModel.objects.get(short_code=short_code)
        except ShortenedUrlModel.DoesNotExist:
            return None


class ShortenedUrlMutation(DjangoModelFormMutation):
    new_url = graphene.Field(ShortenedUrl)

    class Meta:
        form_class = ShortenedUrlForm
        return_field_name = "new_url"


class Mutation(graphene.ObjectType):
    shorten_url = ShortenedUrlMutation.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
