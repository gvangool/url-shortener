from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render

from .models import ShortenedUrl


def redirect_by_short_code(request, short_code):
    try:
        obj = ShortenedUrl.objects.get(short_code=short_code)
    except ShortenedUrl.DoesNotExist:
        raise Http404
    return HttpResponseRedirect(obj.url)
