from django.contrib import admin

from .models import ShortenedUrl


@admin.register(ShortenedUrl)
class ShortenedUrlAdmin(admin.ModelAdmin):
    list_display = ["short_code", "url"]
    readonly_fields = ["short_code"]
