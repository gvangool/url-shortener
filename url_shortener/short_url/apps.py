from django.apps import AppConfig


class ShortUrlConfig(AppConfig):
    name = "url_shortener.short_url"
