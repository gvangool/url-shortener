import json

import pytest
from django.test import TestCase
from django.urls import reverse
from graphene_django.utils.testing import GraphQLTestCase

from .models import ShortenedUrl
from .schema import schema
from .utils import decode_int, encode_int


@pytest.mark.parametrize("i", [1, 42, 4242, 424242, 2 ** 32 - 1])
def test_int_encoding(i):
    assert i == decode_int(encode_int(i))


@pytest.mark.parametrize(
    "value,i", [("Kg", 42), ("Kh", 42), ("AAAAKg==", 42), ("AodXsg", 42424242)]
)
def test_int_decoding(value, i):
    assert i == decode_int(value)


@pytest.mark.parametrize("i", [None, 0, -42, 2 ** 32])
def test_failed_int_encoding(i):
    with pytest.raises(OverflowError):
        assert encode_int(i)


@pytest.mark.parametrize("value", ["pytest"])
def test_failed_int_decoding(value):
    assert isinstance(decode_int(value), int)


@pytest.mark.django_db
def test_model():
    data = ShortenedUrl(url="https://www.google.com")
    assert data.short_code == ""
    data.save()
    short_code = data.short_code
    assert len(short_code) > 1
    obj = ShortenedUrl.objects.get(short_code=short_code)
    assert obj.url == "https://www.google.com"


@pytest.mark.django_db
def test_redirect(client):
    obj = ShortenedUrl.objects.create(url="https://www.google.com")
    response = client.get(f"/{obj.short_code}")
    assert response.status_code == 302
    assert response["Location"] == "https://www.google.com"


class ShortenedUrlSchemaTestCase(GraphQLTestCase):
    GRAPHQL_URL = reverse("graphql")
    GRAPHQL_SCHEMA = schema

    def test_query(self):
        obj = ShortenedUrl.objects.create(url="https://www.google.com")
        response = self.query(
            """
            query {
                url(shortCode: "%s") {
                    shortCode
                    url
                }
            }
            """
            % obj.short_code
        )
        assert response.status_code == 200
        content = json.loads(response.content)
        assert "errors" not in content
        short_url = content.get("data", {}).get("url")
        assert short_url is not None
        assert short_url["shortCode"] == obj.short_code
        assert short_url["url"] == "https://www.google.com"

    def test_mutation_valid(self):
        response = self.query(
            """
            mutation {
              shortenUrl(input: {url: "https://www.google.be"}) {
                newUrl {
                  shortCode
                  url
                }
                errors {
                  field
                  messages
                }
              }
            }
            """
        )
        assert response.status_code == 200
        content = json.loads(response.content)
        assert "errors" not in content
        data = content["data"]["shortenUrl"]
        assert data["errors"] == []
        short_url = data["newUrl"]
        assert short_url["url"] == "https://www.google.be"
        assert len(short_url["shortCode"]) > 1
        assert ShortenedUrl.objects.get(short_code=short_url["shortCode"]) is not None
