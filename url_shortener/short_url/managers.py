from django.db import models

from .utils import decode_int


class ShortenedUrlQuerySet(models.QuerySet):
    def get(self, *args, **kwargs):
        short_code = kwargs.get("short_code")
        if short_code:
            pk = decode_int(short_code)
            return super().get(pk=pk)
        return super().get(*args, **kwargs)
