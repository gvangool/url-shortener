URL Shortener
=============
Build a URL shortening service, which includes a simple GraphQL API. The API
should provide the following functionality, and does not require any user
authentication:

- Submit a URL and get back a shortened URL. For example, submit
  ``http://www.google.ca/somelongpath`` and receive ``http://localhost/A7dw`` as the
  shortened URL
- Fetch the actual URL for a shortened tag. For example, passing in ``"A7dw"``
  from the example above would return ``http://www.google.ca/somelongpath`` in the
  API response

In addition, visiting the shortened URL (e.g. ``http://localhost/A7dw`` from the
example above) would redirect the browser to the real URL.

Installation
------------
You can run the project locally, by using `pipenv <https://docs.pipenv.org/>`_
and running ``pipenv install``.

.. note:: If pipenv gives errors while creating the virtualenv, try running
   with ``pipenv --three --python=$(which python3)``

Testing
-------
For testing, I've used pytest.

.. code:: bash

   pipenv install --dev
   pipenv run python -m pytest

Demo
----
The demo application can be run via pipenv as well:

.. code:: bash

   pipenv run python manage.py migrate
   pipenv run python manage.py runserver

This will keep everything in a local sqlite database, setting ``DATABASE_URL``
environment variable (parsed via `dj-database-url
<https://github.com/jacobian/dj-database-url>`_) will switch the database.


Adding data::

  mutation {
    shortenUrl(input: {url: "https://jobs.lever.co/7geese/19c319a5-6340-4dd3-a35d-751825dabaea"}) {
      newUrl {
        shortCode
        url
      }
      errors {
        field
        messages
      }
    }
  }

Viewing target::

  query {
    url(shortCode: "AQ") {
      url
    }
  }

Redirect view::

  https://127.0.0.1:8000/AQ

Configuration
-------------
There are environment variables available for configuration

- ``DJANGO_CONFIG``: allowed values ``DEV``, ``STAGING``, ``PRODUCTION``
- ``ALLOWED_HOSTS``: space separated list of the allowed domain names. This
  should be set if ``DJANGO_CONFIG`` is not ``DEV``.
- ``SECRET_KEY``: replace the used ``SECRET_KEY`` (is required if
  ``DJANGO_CONFIG`` is not ``DEV``)
- ``DATABASE_URL``: will set ``DATABASES``, parsed via `dj-database-url
  <https://github.com/jacobian/dj-database-url>`_
